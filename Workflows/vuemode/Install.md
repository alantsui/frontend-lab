# How to install vueJS
## By copying 
```html
<script src="https://unpkg.com/vue"></script>
```

## By installing globally
```sh
npm install --global @vue/cli
yarn global add @vue/cli
```

## Installing bulma 
```sh
npm install bulma --save
```

## Install Sass Loader
```sh 
npm install -D sass-loader sass
```
