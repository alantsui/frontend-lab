## Palindrome Checker
1. Return true if the given string is a palindrome. Otherwise, return false.
2. A palindrome is a word or sentence that's spelled the same way both forward and backward, ignoring punctuation, case, and spacing.
### Note
1. You'll need to remove all __non-alphanumeric__ characters (punctuation, spaces and symbols) and turn everything into the same case (lower or upper case) in order to check for palindromes.
2. We'll pass strings with varying formats, such as "racecar", "RaceCar", and "race CAR" among others.
3. We'll also pass strings with special symbols, such as "2A3*3a2", "2A3 3a2", and "2_A3*3#A2".