var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');

function saass(){
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("src/css"))
        .pipe(browserSync.stream());
}

exports.saass = saass

function js(){
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/umd/popper.min.js'])
        .pipe(gulp.dest("src/js"))
        .pipe(browserSync.stream());
}

exports.js = js

function serve(done){
    browserSync.init({
        server: "./src"  
    });
    done();
}

exports.serve = serve

function watch(done){
    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], saass);
    gulp.watch("src/*.html").on('change', browserSync.reload);
    done();
}

exports.watch = gulp.series(saass,watch);

exports.default = gulp.series(js,exports.watch,serve)