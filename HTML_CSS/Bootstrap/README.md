1. Install `Node`
```sh
node -v
```

2. Install npm plugins
```sh
npm install --global gulp-cli
npm install gulp browser-sync gulp-sass --save-dev
```
* Gulp:javascipt task runner
* Sync browser when change
* gulp-sass: compile sass to css

3. Install bootstrap
```sh
npm install bootstrap jquery popper.js --save
```

4. Create this folder structure
_Create with `mkdir -p path`
```
/src
    /assets
    /css
    /js
    /scss
```

5. Gulp:`Did you forget to signal async completion?`
* Solution 1: rename functions
* Solution 2: add `done` in functon parameters and `done()` at the end of functions

6. Watch has to be a function
 ```js
  //Change these syntax
  gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], ['sass']);
  //To gulp 4 Format
gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], sass);
  ```
7. Gulp:Other errors
> https://www.sitepoint.com/how-to-migrate-to-gulp-4/

8. Emmet 
> https://docs.emmet.io/abbreviations/syntax/

9. Bootstrap
> https://coursetro.com/posts/code/130/Learn-Bootstrap-4-Final-in-2018-with-our-Free-Crash-Course