# CSS cheatsheet

1. Selectors
```css
.class {
}
#id {
}
```

2. Fonts
```css
.class{
    /* If no Helvetica, degrade to sans-serif */
    font-family:Helvetica,sans-serif;
    font-size:30px;
    color:red;
}
```

3. Images sizing
```css
.class {
    width:500px;
}
```

4. Background
```css
.class{
    background-color:green;
}
```

5. Border
```css
.class {
    border-color:red;
    border-width:5px;
    border-style:solid;
    border-radius:10px;
}
```

6. Padding (space between _Border_ and _text_)
```css
.class{
    padding:20px
}
```

7. 