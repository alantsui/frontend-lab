# HTML & CSS Journey

## Day1
### Comming Soon
> Skill: HTML, CSS
<div>
  <img width="505" alt="Day1_1" src="https://user-images.githubusercontent.com/33037271/55958229-bbfab700-5c9a-11e9-864e-e700ecba8029.png">
  <img width="300" alt="Day1_2" src="https://user-images.githubusercontent.com/33037271/55958234-c026d480-5c9a-11e9-94c3-5cec5a040e8f.png">
 </div>


## Day2
### Image Gallery Loop and animation
> Skill: HTML, CSS
<div>
  <img width="300" alt="Day2_1" src="https://user-images.githubusercontent.com/33037271/55958242-c2892e80-5c9a-11e9-9913-4f83f33b6312.png">
  <img width="300" alt="Day2_2" src="https://user-images.githubusercontent.com/33037271/55958247-c3ba5b80-5c9a-11e9-910c-1a3072f19bf9.png">
  </div>

## Day3
### Video Play in website
> Skill: HTML, CSS, JS

![Apr-11-2019 21-04-13](https://user-images.githubusercontent.com/33037271/55959461-696eca00-5c9d-11e9-874c-737091872c5d.gif)

## Day4
### Study Notes on Midterm of a course
> Skill: vuePress

> Website: https://heihaho.github.io/UGEA2190/

<div>
  <img width="300" alt="Screenshot 2019-04-11 at 9 19 10 PM" src="https://user-images.githubusercontent.com/33037271/55960529-9328f080-5c9f-11e9-82bb-71d722a21bdb.png">
<img width="300" alt="Screenshot 2019-04-11 at 9 18 58 PM" src="https://user-images.githubusercontent.com/33037271/55960534-945a1d80-5c9f-11e9-8822-dd15699b9e03.png">
<img width="300" alt="Screenshot 2019-04-11 at 9 18 50 PM" src="https://user-images.githubusercontent.com/33037271/55960535-958b4a80-5c9f-11e9-8bb8-6bb2d632916f.png">
</div>
